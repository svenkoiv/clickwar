package koiv.sven.clickwar;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sven Kõiv on 30.04.2017.
 */

public class GameLayoutView extends SurfaceView implements Runnable {

    private final int VICTORY_COUNT = 20;
    private int mPlayerOneClicks = 0;
    private int mPlayerTwoClicks = 0;
    private Thread mDrawThread;
    private Thread mCircleAnimationThread;
    private Thread mOpponentCircleAnimationThread;
    private Boolean mCanDraw = false;
    private Canvas mCanvas;
    private SurfaceHolder mSurfaceHolder;
    private Circle mCircle;
    private Circle mOpponentCircle;
    private DatabaseReference mRoomReference;
    private DatabaseReference mRoomReferenceTwo;
    private FirebaseAuth mAuth;
    private CircleClickAnimation mCircleClickAnimation;
    private CircleClickAnimation mOpponentCircleClickAnimation;
    private float mPixelDensity;
    private FirebaseTools mFirebaseTools;
    private Brush mBrush;
    private boolean isCurrentUserPlayerOne;
    private DrawGame mDrawGame;
    private StorageCircles mStorageCircles;
    private boolean isSetup = false;
    private boolean isMovingEffectActive = false;
    private String mEffectName;
    private BitmapLocation mBitmapLocation;
    private SoundHelper mSoundHelper;
    private boolean canGenerateEffect = true;
    private boolean isGameOver = false;
    private boolean canPlay = false;
    private boolean playerOneReady = false;
    private boolean playerTwoReady = false;
    private ValueEventListener mListenerGameReady;

    public GameLayoutView(Context context, DatabaseReference mRoomReference) {
        super(context);
        this.mRoomReference = mRoomReference;
        mRoomReferenceTwo = mRoomReference;
        mSurfaceHolder = getHolder();
        mAuth = FirebaseAuth.getInstance();
        mCircleClickAnimation = new CircleClickAnimation();
        mOpponentCircleClickAnimation = new CircleClickAnimation();
        mPixelDensity = getResources().getDisplayMetrics().density;
        mFirebaseTools = new FirebaseTools();
        mBrush = new Brush(getResources().getDisplayMetrics().density);
        mDrawGame = new DrawGame(getResources(), mRoomReference);
        mSoundHelper = new SoundHelper(context);

        listenerForGameReady();
        isCurrentUserPlayerOne();
        setRoomListener();
        createClickCircles();
    }

    private void sendReadToPlay() {
        if (isCurrentUserPlayerOne) mRoomReference.child("playerOneStatus").setValue(Room.PLAY);
        else mRoomReference.child("playerTwoStatus").setValue(Room.PLAY);
    }

    private void tryToStartGame() {
        if (playerOneReady && playerTwoReady) {
            mRoomReferenceTwo.removeEventListener(mListenerGameReady);
            canPlay = true;
        }
    }

    @Override
    public void run() {
        while (mCanDraw) {
            if (!mSurfaceHolder.getSurface().isValid()) {
                continue;
            }
            mCanvas = mSurfaceHolder.lockCanvas();
            drawOnCanvas();
            mSurfaceHolder.unlockCanvasAndPost(mCanvas);
        }
    }

    private void goGameOverActivity() {
        isGameOver = true;
        Intent intent = new Intent().setClass(getContext(), GameOver.class);
        intent.putExtra("room", mRoomReference.getKey());
        getContext().startActivity(intent);
        ((Activity) getContext()).finish();
    }

    private void listenerForGameReady() {
        mListenerGameReady = mRoomReferenceTwo.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot != null) {
                    Room room = dataSnapshot.getValue(Room.class);
                    Log.d("gameLayout", "playerOneStatus" + room.playerOneStatus);
                    Log.d("gameLayout", "playerTwoStatus" + room.playerTwoStatus);
                    if (room.playerTwoStatus.equals(Room.PLAY)) {
                        playerTwoReady = true;
                        tryToStartGame();
                    }
                    if (room.playerOneStatus.equals(Room.PLAY)) {
                        playerOneReady = true;
                        tryToStartGame();
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {}
        });
    }

    private void isCurrentUserPlayerOne() {
        mRoomReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot != null) {
                    Room room = dataSnapshot.getValue(Room.class);
                    isCurrentUserPlayerOne = (room.playerOne.equals(mAuth.getCurrentUser().getUid()));
                    sendReadToPlay();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    private void createClickCircles() {
        int circleRadius = 50;
        mCircle = new Circle(0, 0, mPixelDensity, circleRadius);
        mOpponentCircle = new Circle(0, 0, mPixelDensity, circleRadius);
    }

    private void drawOnCanvas() {
        mCanvas.drawPaint(mBrush.getBackgroundColorInt()); // Draw background
        setupOnlyOnce(); // Setup starting locations for drawing

        drawMyCircleChildren();
        drawOpponentCircleChildren();
        drawCirclesForTouch();
        drawReadyOrWaiting();

        if (canPlay) {
            drawPlayersClickCount();
            tryToGenerateEffect(); // Tries to generate random effect
            drawAnimation(); // Draws animation which is happening
            drawMovingEffect(); // Draws effect which is moving
        }
    }

    private void setupOnlyOnce() {
        if (!isSetup) {
            mCircle.setLocationY(mCanvas.getHeight(), false);
            mCircle.setLocationX(mCanvas.getWidth() / 2);
            mOpponentCircle.setLocationY(0, true);
            mOpponentCircle.setLocationX(mCanvas.getWidth() / 2);
            mStorageCircles = new StorageCircles(mCircle, mOpponentCircle, mPixelDensity, getWidth());
            isSetup = true;
        }
    }

    /**
     * Method try toToGenerateEffect.
     * - needs to be playerOne to generate.
     * - there shouldn't be active moving effect.
     * - adds random effect to database
     */
    private void tryToGenerateEffect() {
        if (canGenerateEffect) {
            if (isCurrentUserPlayerOne && !isMovingEffectActive) {
                mDrawGame.tryToGenerateEffect();
                if (mDrawGame.isEffectWithRandomTimeGenerated()) {
                    canGenerateEffect = false;
                    mRoomReference.child("effect").setValue(mDrawGame.getGeneratedEffect());
                }
            }
        }
    }

    private void drawAnimation() {
        mDrawGame.drawAnimation(mCanvas);
    }

    private void drawMovingEffect() {
        if (isMovingEffectActive) {
            mBitmapLocation = setBitMapLocation(mCanvas, mBitmapLocation);
            mDrawGame.drawMovingEffect(mCanvas, mBitmapLocation);
        }
    }

    private BitmapLocation setBitMapLocation(Canvas canvas, BitmapLocation bitmapLocation) {
        if (bitmapLocation.getLocationX() == null && bitmapLocation.getLocationY() == null) {
            bitmapLocation.setLocationX((canvas.getWidth() / 2) - bitmapLocation.getWidth());
            bitmapLocation.setLocationY((canvas.getHeight() / 2) - bitmapLocation.getHeight());
        }
        return bitmapLocation;
    }

    private void drawCirclesForTouch() {
        Paint strokePaint = mCircleClickAnimation.getCalculatedPaint();
        strokePaint.setStrokeWidth(mCircle.getCircleRadius() / 5);
        Paint strokePaintOpponent = mOpponentCircleClickAnimation.getCalculatedPaint();
        strokePaintOpponent.setStrokeWidth(mOpponentCircle.getCircleRadius() / 5);
        mCanvas.drawCircle(mCircle.getLocationX(), mCircle.getLocationY(), mCircle.getCircleRadius(), strokePaint);
        mCanvas.drawCircle(mOpponentCircle.getLocationX(), mOpponentCircle.getLocationY(), mOpponentCircle.getCircleRadius(), strokePaintOpponent);
        mCanvas.drawCircle(mCircle.getLocationX(), mCircle.getLocationY(), mCircle.getCircleRadius(), mBrush.getMyCirclePaint());
        mCanvas.drawCircle(mOpponentCircle.getLocationX(), mOpponentCircle.getLocationY(), mOpponentCircle.getCircleRadius(), mBrush.getOpponentPaint());
    }

    private void drawPlayersClickCount() {
        Paint textPaint = mBrush.getTextPaint(30);
        int fromBaseLineToCenter = (int) ((textPaint.descent() + textPaint.ascent()) / 2);
        if (isCurrentUserPlayerOne) {
            mCanvas.drawText(mPlayerOneClicks + "", mCircle.getLocationX(), mCircle.getLocationY() - fromBaseLineToCenter, textPaint);
            mCanvas.drawText(mPlayerTwoClicks + "", mOpponentCircle.getLocationX(), mOpponentCircle.getLocationY() - fromBaseLineToCenter, textPaint);
        } else {
            mCanvas.drawText(mPlayerTwoClicks + "", mCircle.getLocationX(), mCircle.getLocationY() - fromBaseLineToCenter, textPaint);
            mCanvas.drawText(mPlayerOneClicks + "", mOpponentCircle.getLocationX(), mOpponentCircle.getLocationY() - fromBaseLineToCenter, textPaint);
        }
    }

    private void drawReadyOrWaiting() {
        if (!canPlay) {
            Paint textPaint = mBrush.getStartTextPaint(30);
            int fromBaseLineToCenter = (int) ((textPaint.descent() + textPaint.ascent()) / 2);
            if (isCurrentUserPlayerOne) {
                if (playerOneReady) {
                    mCanvas.drawText("Ready", mCircle.getLocationX(), mCircle.getLocationY() - fromBaseLineToCenter, textPaint);
                } else mCanvas.drawText("...", mCircle.getLocationX(), mCircle.getLocationY() - fromBaseLineToCenter, textPaint);
                if (playerTwoReady) {
                    mCanvas.drawText("Ready", mOpponentCircle.getLocationX(), mOpponentCircle.getLocationY() - fromBaseLineToCenter, textPaint);
                } else mCanvas.drawText("...", mOpponentCircle.getLocationX(), mOpponentCircle.getLocationY() - fromBaseLineToCenter, textPaint);;
            } else {
                if (playerTwoReady) {
                    mCanvas.drawText("Ready", mCircle.getLocationX(), mCircle.getLocationY() - fromBaseLineToCenter, textPaint);
                } else mCanvas.drawText("...", mCircle.getLocationX(), mCircle.getLocationY() - fromBaseLineToCenter, textPaint);
                if (playerOneReady) {
                    mCanvas.drawText("Ready", mOpponentCircle.getLocationX(), mOpponentCircle.getLocationY() - fromBaseLineToCenter, textPaint);
                } else mCanvas.drawText("...", mOpponentCircle.getLocationX(), mOpponentCircle.getLocationY() - fromBaseLineToCenter, textPaint);
            }
        }
    }

    private void drawMyCircleChildren() {
        List<Circle> myCircleChildren;
        synchronized (myCircleChildren = mStorageCircles.getMyCircleChildren()) {
            List<Circle> circlesToRemove = new ArrayList<>();
            if (myCircleChildren.size() > 0) {
                for (Circle child : myCircleChildren) {
                    child.moveUp();
                    if (validateMyChildLocation(child)) {
                        mCanvas.drawCircle(child.getLocationX(), child.getLocationY(), child.getCircleRadius(), mBrush.getMyCirclePaint());
                    } else {
                        circlesToRemove.add(child);
                    }
                }
            }
            mStorageCircles.setMyCircleChildren(mStorageCircles.removeCirclesFromList(circlesToRemove, myCircleChildren));
        }
    }

    private void drawOpponentCircleChildren() {
        List<Circle> opponentCircleChildren;
        synchronized (opponentCircleChildren = mStorageCircles.getOpponentCircleChildren()) {
            List<Circle> circlesToRemove = new ArrayList<>();
            if (opponentCircleChildren.size() > 0) {
                for (Circle child : opponentCircleChildren) {
                    child.moveDown();
                    if (validateOpponentChildLocation(child)) {
                        mCanvas.drawCircle(child.getLocationX(), child.getLocationY(), child.getCircleRadius(), mBrush.getOpponentPaint());
                    } else {
                        circlesToRemove.add(child);
                    }
                }
            }
            mStorageCircles.setOpponentCircleChildren(mStorageCircles.removeCirclesFromList(circlesToRemove, opponentCircleChildren));
        }
    }

    private boolean validateOpponentChildLocation(Circle child) {
        final int RADIUS = 5;
        int clickDifference = getOpponentClickDifference();
        int myCircleBirthHeight = (int) mCircle.getLocationY() - mCircle.getCircleRadius() + mCircle.toDensity(RADIUS);
        int height = clickDifference * ((myCircleBirthHeight - (mCanvas.getHeight() / 2)) / VICTORY_COUNT);
        int childCircleHeight = (mCanvas.getHeight() / 2) + height;
        return ((int) child.getLocationY() <= childCircleHeight && myCircleBirthHeight >= childCircleHeight);
    }

    private boolean validateMyChildLocation(Circle child) {
        final int RADIUS = 5;
        int clickDifference = getMyClickDifference();
        int opponentBirthCircleHeight = (int) mOpponentCircle.getLocationY() + mCircle.getCircleRadius() - mCircle.toDensity(RADIUS);
        int height = clickDifference * (((mCanvas.getHeight() / 2) - opponentBirthCircleHeight) / VICTORY_COUNT);
        int childCircleHeight = (mCanvas.getHeight() / 2) - height;
        return ((int) child.getLocationY() >= childCircleHeight && opponentBirthCircleHeight <= childCircleHeight);
    }

    private int getMyClickDifference() {
        if (isCurrentUserPlayerOne) return mPlayerOneClicks - mPlayerTwoClicks;
        else return mPlayerTwoClicks - mPlayerOneClicks;
    }

    private int getOpponentClickDifference() {
        if (isCurrentUserPlayerOne) return mPlayerTwoClicks - mPlayerOneClicks;
        else return mPlayerOneClicks - mPlayerTwoClicks;
    }

    @Override
    public synchronized boolean onTouchEvent(MotionEvent ev) {
        if (canPlay) {
            switch (ev.getAction()) {
                case MotionEvent.ACTION_DOWN: {
                    int x = (int) ev.getX();
                    int y = (int) ev.getY();
                    if (mCircle.isCircleTouched(x, y)) {
                        if (!mDrawGame.getIsOpponent() || !mDrawGame.getCanDraw()) {
                            mCircleClickAnimation.doAnimation();
                            mSoundHelper.clickSound();
                            sendClickToDatabase();
                        }
                    }
                    if (mDrawGame.isMovingEffectTouched(x, y)) {
                        sendEffectHasBeenTouched();
                        if (mEffectName != null) {
                            if (mEffectName.equals("ten")) {
                                for (int i = 0; i < 10; i++, sendClickToDatabase()) ;
                                mRoomReference.child("effect").removeValue();
                            }
                        }

                    }
                    if (mDrawGame.isAnimationBeingTouched(x, y)) {
                        //pass
                    }
                    break;
                }
            }
        }
        return true;
    }

    private void sendEffectHasBeenTouched() {
        mRoomReference.child("effect").child("playerClicked").runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData mutableData) {
                if (mutableData.getValue() != null) {
                    mutableData.setValue(mAuth.getCurrentUser().getUid());
                    return Transaction.success(mutableData);
                }
                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {
            }
        });
    }

    private void setRoomListener() {
        mRoomReference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                movingEffectAdded(dataSnapshot);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                listenRoomChanges(dataSnapshot);

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                canGenerateEffect = true;
                mDrawGame.resetAnimation();
                mDrawGame.resetEverything();
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    private void movingEffectAdded(DataSnapshot dataSnapshot) {
        if (dataSnapshot.getKey().equals("effect")) {
            Effect effect = dataSnapshot.getValue(Effect.class);
            int angle = Integer.parseInt(effect.angle);
            Bitmap bitmap = mDrawGame.getBitmap(effect.name);
            mEffectName = effect.name;
            mBitmapLocation = new BitmapLocation(bitmap, angle);
            mDrawGame.setListenerForBitmapLocation(mBitmapLocation);
            isMovingEffectActive = true;
        }
    }

    private void listenRoomChanges(DataSnapshot dataSnapshot) {
        String key = dataSnapshot.getKey();
        String value = dataSnapshot.getValue().toString();
        if (key.equals("playerOneClickAmount")) playerOneMadeClick(value);
        else if (dataSnapshot.getKey().equals("playerTwoClickAmount")) playerTwoMadeClick(value);
        else if (dataSnapshot.getKey().equals("effect")) {
            Effect effect = dataSnapshot.getValue(Effect.class);
            if (!effect.playerClicked.equals("")) {
                isMovingEffectActive = false;
                mBitmapLocation = null;
                if (isEffectClickedByMe(effect.playerClicked)) {
                    mDrawGame.setAnimationToDraw(effect.name, false, mOpponentCircle);
                } else {
                    mDrawGame.setAnimationToDraw(effect.name, true, mCircle);
                }
                mDrawGame.resetEverything();
            }
        } else if (dataSnapshot.getKey().equals("roomStatus")) {
            if (dataSnapshot.getValue().toString().equals(Room.GAME_OVER)) goGameOverActivity();
        }
    }

    private boolean isEffectClickedByMe(String user) {
        return user.equals(mAuth.getCurrentUser().getUid());
    }

    @SuppressWarnings("StatementWithEmptyBody")
    private void playerTwoMadeClick(String value) {
        int difference = Integer.parseInt(value) - mPlayerTwoClicks;
        if (isCurrentUserPlayerOne) {
            mOpponentCircleClickAnimation.doAnimation();
            for (int i = 0; i < difference; i++, mStorageCircles.giveBirthToOpponentCircle()) ;
        } else {
            for (int i = 0; i < difference; i++, mStorageCircles.giveBirthToMyChildCircle()) ;
        }
        mPlayerTwoClicks = Integer.parseInt(value);
        validateGameOver();
    }

    @SuppressWarnings("StatementWithEmptyBody")
    private void playerOneMadeClick(String value) {
        int difference = Integer.parseInt(value) - mPlayerOneClicks;
        if (isCurrentUserPlayerOne) {
            for (int i = 0; i < difference; i++, mStorageCircles.giveBirthToMyChildCircle()) ;
        } else {
            mOpponentCircleClickAnimation.doAnimation();
            for (int i = 0; i < difference; i++, mStorageCircles.giveBirthToOpponentCircle()) ;
        }
        mPlayerOneClicks = Integer.parseInt(value);
        validateGameOver();
    }

    private void validateGameOver() {
        if (Math.abs(mPlayerOneClicks - mPlayerTwoClicks) >= VICTORY_COUNT) {
            mRoomReference.child("roomStatus").setValue(Room.GAME_OVER);
        }
    }

    /**
     * Method sendClickToDatabase.
     * - Sends click with Transaction to database.
     * - Click will succeed.
     */
    private void sendClickToDatabase() {
        if (isCurrentUserPlayerOne)
            mRoomReference.child("playerOneClickAmount").runTransaction(mFirebaseTools.getSendClickTransactionPlayerOne());
        else
            mRoomReference.child("playerTwoClickAmount").runTransaction(mFirebaseTools.getSendClickTransactionPlayerTwo());
    }

    /**
     * Method pause
     * - Removes threads
     */
    protected void pause() {
        mCanDraw = false;
        mCircleClickAnimation.stopRunnable();
        mOpponentCircleClickAnimation.stopRunnable();
        while (true) {
            try {
                mCircleAnimationThread.interrupt();
                mOpponentCircleAnimationThread.interrupt();
                mDrawThread.join();
                break;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        mDrawThread = null;
        mCircleAnimationThread = null;
        mOpponentCircleAnimationThread = null;
        mSoundHelper.release();
        if (!isGameOver) {
            mRoomReference.child("playerLeft").setValue(mAuth.getCurrentUser().getUid());
            mRoomReference.child("roomStatus").setValue(Room.GAME_OVER);
        }
    }

    /**
     * Method resume
     * - Starts threads
     */
    protected void resume() {
        mCanDraw = true;
        mDrawThread = new Thread(this);
        mDrawThread.start();
        mCircleAnimationThread = new Thread(mCircleClickAnimation);
        mCircleAnimationThread.start();
        mOpponentCircleAnimationThread = new Thread(mOpponentCircleClickAnimation);
        mOpponentCircleAnimationThread.start();
        mSoundHelper = new SoundHelper(getContext());
    }
}

package koiv.sven.clickwar;

import android.graphics.Paint;

import java.util.Date;

/**
 * Created by Sven Kõiv on 13.05.2017.
 */

class CircleClickAnimation implements Runnable {

    private long differenceBetweenClicks = 0;
    private volatile long dateRecentClick;
    private volatile boolean isAnimationCompleted = false;
    private volatile boolean isRunning = true;

    Paint getCalculatedPaint() {
        Paint strokePaint = new Paint();
        strokePaint.setStyle(Paint.Style.STROKE);
        strokePaint.setAntiAlias(true);
        strokePaint.setARGB(255 - (int) differenceBetweenClicks / 5, 255 - (int) differenceBetweenClicks / 5, 255 - (int) differenceBetweenClicks / 5, 255);
        return strokePaint;
    }

    void doAnimation() {
        if (isAnimationCompleted) {
            synchronized (this) {
                notify();
            }
        } else dateRecentClick = new Date().getTime();
    }

    @Override
    public void run() {
        while (isRunning) {
            differenceBetweenClicks = 0;
            isAnimationCompleted = true;
            synchronized (this) {
                try {
                    wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                isAnimationCompleted = false;
                dateRecentClick = new Date().getTime();
                while ((differenceBetweenClicks = new Date().getTime() - dateRecentClick) <= 500) {
                    try {
                        Thread.sleep(25);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    void stopRunnable() {
        isRunning = false;
    }
}

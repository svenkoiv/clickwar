package koiv.sven.clickwar;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;

import com.google.firebase.database.DatabaseReference;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

/**
 * Created by Sven Kõiv on 15.05.2017.
 */

class DrawGame {

    private long mEffectGenerationTime = 0;

    private BitmapLocation mMovingEffect;
    private DatabaseReference mRoomReference;
    private BitmapLocation mAnimationEffect;
    private Effect mGeneratedEffect;
    private HashMap<String, Bitmap> mBitmapStorage = new HashMap<>();
    private List<String> mStorageKeys = new ArrayList<>(Arrays.asList("ten", "snowflake"));
    private List<Integer> randomAngles = new ArrayList<>(Arrays.asList(35, 60, 95, 130, 165, 200, 235, 270, 305, 340));

    private boolean isOpponent;
    private boolean isEffectGenerated = false;
    private boolean isTimeGenerated = false;
    private boolean canDoAnimation = false;
    private int iceCounter = 0;

    DrawGame(Resources resources, DatabaseReference mRoomReference) {
        this.mRoomReference = mRoomReference;
        loadBitmapsToStorage(resources);
    }

    private void loadBitmapsToStorage(Resources resources) {
        mBitmapStorage.put("ten", BitmapFactory.decodeResource(resources, R.drawable.ten));
        mBitmapStorage.put("frozen", BitmapFactory.decodeResource(resources, R.drawable.frozen));
        mBitmapStorage.put("snowflake", BitmapFactory.decodeResource(resources, R.drawable.snowflake));
    }

    void tryToGenerateEffect() {
        if (!isTimeGenerated) {
            mEffectGenerationTime = new Random().nextInt(7000) + 3000 + new Date().getTime();
            isTimeGenerated = true;
        } else if (!isEffectGenerated && mEffectGenerationTime < new Date().getTime()) {
            Random random = new Random();
            String randomKey = mStorageKeys.get(random.nextInt(mStorageKeys.size()));
            int randomAngle = randomAngles.get(random.nextInt(randomAngles.size()));
            mGeneratedEffect = new Effect(randomKey,"" + randomAngle);
            isEffectGenerated = true;
        }
    }

    Effect getGeneratedEffect() {
        return mGeneratedEffect;
    }

    Bitmap getBitmap(String name) {
        switch (name) {
            case "ten":
                return mBitmapStorage.get("ten");
            case "snowflake":
                return mBitmapStorage.get("snowflake");
            case "frozen":
                return mBitmapStorage.get("frozen");
        }
        return null;
    }

    boolean isEffectWithRandomTimeGenerated() {
        return isTimeGenerated && isEffectGenerated;
    }

    void drawMovingEffect(Canvas canvas, BitmapLocation bitmapLocation) {
        bitmapLocation.moveBitmap(canvas.getWidth(), canvas.getHeight());
        canvas.drawBitmap(bitmapLocation.getBitmap(), bitmapLocation.getLocationX(), bitmapLocation.getLocationY(), null);
    }

    void drawAnimation(Canvas canvas) {
        if (canDoAnimation) {
            canvas.drawBitmap(mAnimationEffect.getBitmap(), mAnimationEffect.getLocationX(), mAnimationEffect.getLocationY(), null);
        }
    }

    void setListenerForBitmapLocation(BitmapLocation bitmapLocation) {
        mMovingEffect = bitmapLocation;
    }

    boolean isMovingEffectTouched(int clickX, int clickY) {
        if (mMovingEffect != null && mMovingEffect.getLocationX() != null) {
            if (clickX >= mMovingEffect.getLocationX() &&
                    clickX <= mMovingEffect.getLocationX() + mMovingEffect.getWidth() &&
                    clickY >= mMovingEffect.getLocationY() &&
                    clickY <= mMovingEffect.getLocationY() + mMovingEffect.getHeight()) {
                return true;
            }
        }
        return false;
    }

    boolean isAnimationBeingTouched(int clickX, int clickY) {
        if (mAnimationEffect != null && mAnimationEffect.getLocationX() != null) {
            if (clickX >= mAnimationEffect.getLocationX() &&
                    clickX <= mAnimationEffect.getLocationX() + mAnimationEffect.getWidth() &&
                    clickY >= mAnimationEffect.getLocationY() &&
                    clickY <= mAnimationEffect.getLocationY() + mAnimationEffect.getHeight()) {
                iceCounter++;
                tryToFinishAnimation();
                return true;
            }
        }
        return false;
    }

    void resetAnimation() {
        iceCounter = 0;
        canDoAnimation = false;
        mAnimationEffect = null;
    }

    private void tryToFinishAnimation() {
        if (iceCounter == 4) {
            mRoomReference.child("effect").removeValue();
        }
    }

    boolean getCanDraw() {
        return canDoAnimation;
    }

    boolean getIsOpponent() {
        return isOpponent;
    }

    void resetEverything() {
        isTimeGenerated = false;
        mGeneratedEffect = null;
        mMovingEffect = null;
        isEffectGenerated = false;
    }

    void setAnimationToDraw(String name, boolean isOpponent, Circle circle) {
        this.isOpponent = isOpponent;
        if (name.equals("snowflake")) {
            Bitmap bitmap = getBitmap("frozen");
            BitmapLocation bitmapLocation = new BitmapLocation(bitmap, 0);
            bitmapLocation.setLocationX((int)circle.getLocationX() - (bitmap.getWidth() / 2));
            bitmapLocation.setLocationY((int)circle.getLocationY() - (bitmap.getHeight() / 2));
            mAnimationEffect = bitmapLocation;
            canDoAnimation = true;
        }
    }
}

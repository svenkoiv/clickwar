package koiv.sven.clickwar;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by Sven Kõiv on 28.04.2017.
 */

@IgnoreExtraProperties
class User {

    public String username;
    public String email;
    public String gamesAmount;
    public String gamesWon;
    public String gamesLost;
    public String photoUrl;

    public User() {}

    public User(String username, String email, String gamesAmount, String gamesWon, String gamesLost, String photoUrl) {
        this.photoUrl = photoUrl;
        this.username = username;
        this.email = email;
        this.gamesAmount = gamesAmount;
        this.gamesWon = gamesWon;
        this.gamesLost = gamesLost;
    }
}

package koiv.sven.clickwar;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

public class GameOver extends AppCompatActivity {

    private String TAG = "GameOver";
    private DatabaseReference mRoomReference;
    private FirebaseAuth mAuth;
    private Room room;
    private User myUser;
    private User opponentUser;
    private TextView greeting;
    private ImageView myProfile;
    private TextView myName;
    private TextView myClicks;
    private TextView myWins;
    private TextView myLosses;

    private ImageView opponentProfile;
    private TextView opponentName;
    private TextView opponentClicks;
    private TextView opponentWins;
    private TextView opponentLosses;
    private boolean getPersonalInfo1 = false;
    private boolean getPersonalInfo2 = false;
    private boolean getWinnerInfo = false;
    private boolean isCurrentUserPlayerOne = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_over);
        getRoomRefFromIntent();
        mAuth = FirebaseAuth.getInstance();
        getLastGameInfo();
        setViews();
    }

    private void setViews() {
        // My views
        myProfile = (ImageView) findViewById(R.id.myProfile);
        myName = (TextView) findViewById(R.id.myName);
        myClicks = (TextView) findViewById(R.id.myClicks);
        myWins = (TextView) findViewById(R.id.myWins);
        myLosses = (TextView) findViewById(R.id.myLosses);

        greeting = (TextView) findViewById(R.id.greeting);

        // OpponentViews
        opponentProfile = (ImageView) findViewById(R.id.opponentProfile);
        opponentName = (TextView) findViewById(R.id.opponentName);
        opponentClicks = (TextView) findViewById(R.id.opponentClicks);
        opponentWins = (TextView) findViewById(R.id.opponentWins);
        opponentLosses = (TextView) findViewById(R.id.opponentLosses);
    }

    private void getRoomRefFromIntent() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            String room = bundle.getString("room");
            Log.d(TAG, room);
            if (room == null) Log.d(TAG, "getRoomRefFromIntent:NoRoom");
            else
                mRoomReference = FirebaseDatabase.getInstance().getReference().child("games").child(room);
        }
    }

    private void getLastGameInfo() {
        mRoomReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() != null) {
                    room = dataSnapshot.getValue(Room.class);
                    getPersonalInformation();
                    validateWinner();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void getPersonalInformation() {
        final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
        databaseReference.child("users").child(room.playerOne).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() != null) {
                    User user = dataSnapshot.getValue(User.class);
                    if (mAuth.getCurrentUser().getUid().equals(room.playerOne)) {
                        myUser = user;
                        isCurrentUserPlayerOne = true;
                    } else opponentUser = user;
                    getPersonalInfo1 = true;
                    displayData();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        databaseReference.child("users").child(room.playerTwo).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() != null) {
                    User user = dataSnapshot.getValue(User.class);
                    if (mAuth.getCurrentUser().getUid().equals(room.playerTwo)) myUser = user;
                    else opponentUser = user;
                    getPersonalInfo2 = true;
                    displayData();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void displayData() {
        if (getPersonalInfo1 && getPersonalInfo2 && getWinnerInfo) {
            if (isCurrentUserPlayerOne) {
                if (!room.playerLeft.equals("")) {
                    if (room.playerLeft.equals(mAuth.getCurrentUser().getUid()))
                        greeting.setText("LOSS");
                    else greeting.setText("WIN");
                } else {
                    if (Integer.parseInt(room.playerOneClickAmount) > Integer.parseInt(room.playerTwoClickAmount)) {
                        greeting.setText("WIN");
                    } else greeting.setText("LOSS");
                }
            } else {
                if (!room.playerLeft.equals("")) {
                    if (room.playerLeft.equals(mAuth.getCurrentUser().getUid()))
                        greeting.setText("LOSS");
                    else greeting.setText("WIN");
                } else {
                    if (Integer.parseInt(room.playerTwoClickAmount) > Integer.parseInt(room.playerOneClickAmount)) {
                        greeting.setText("WIN");
                    } else greeting.setText("LOSS");
                }
            }
            if (greeting.getText().equals("LOSS")) {
                greeting.setTextColor(Color.parseColor("#c32626"));
                greeting.setBackgroundColor(Color.parseColor("#ef5050"));
            } else {
                greeting.setTextColor(Color.parseColor("#1dca19"));
                greeting.setBackgroundColor(Color.parseColor("#8cff89"));
            }

            myName.setText(myUser.username);
            if (isCurrentUserPlayerOne) myClicks.setText(room.playerOneClickAmount);
            else myClicks.setText(room.playerTwoClickAmount);
            myWins.setText(myUser.gamesWon);
            myLosses.setText(myUser.gamesLost);
            setUserProfilePhoto(myUser, myProfile);

            opponentName.setText(opponentUser.username);
            if (isCurrentUserPlayerOne) opponentClicks.setText(room.playerTwoClickAmount);
            else opponentClicks.setText(room.playerOneClickAmount);
            opponentWins.setText(opponentUser.gamesWon);
            opponentLosses.setText(opponentUser.gamesLost);
            setUserProfilePhoto(opponentUser, opponentProfile);
        }
    }

    private void setUserProfilePhoto(User user, ImageView view) {
        if (user.photoUrl != null) {
            Picasso.with(this)
                    .load(user.photoUrl)
                    .transform(new CircleTransform()).into(view);
        }
    }

    private void validateWinner() {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
        Log.d("GameOver", "room.playerLeft " + room.playerLeft);
        if (!room.playerLeft.equals("")) {
            Log.d("GameOver", "playerLeft");
            if (!room.playerLeft.equals(mAuth.getCurrentUser().getUid())) {
                databaseReference.child("users").child(mAuth.getCurrentUser().getUid()).runTransaction(new Transaction.Handler() {
                    @Override
                    public Transaction.Result doTransaction(MutableData mutableData) {
                        if (mutableData.getValue() != null) {
                            int wins = Integer.parseInt(mutableData.child("gamesWon").getValue().toString());
                            mutableData.child("gamesWon").setValue("" + (wins + 1));
                            return Transaction.success(mutableData);
                        }
                        return Transaction.success(mutableData);
                    }

                    @Override
                    public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {

                    }
                });
                databaseReference.child("users").child(room.playerLeft).runTransaction(new Transaction.Handler() {
                    @Override
                    public Transaction.Result doTransaction(MutableData mutableData) {
                        if (mutableData.getValue() != null) {
                            int losses = Integer.parseInt(mutableData.child("gamesLost").getValue().toString());
                            mutableData.child("gamesLost").setValue("" + (losses + 1));
                            return Transaction.success(mutableData);
                        }
                        return Transaction.success(mutableData);
                    }

                    @Override
                    public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {

                    }
                });
            }
        } else {
            Log.d("GameOver", "other ");
            if (room.playerOne.equals(mAuth.getCurrentUser().getUid())) {
                databaseReference.child("users").child(mAuth.getCurrentUser().getUid()).runTransaction(new Transaction.Handler() {
                    @Override
                    public Transaction.Result doTransaction(MutableData mutableData) {
                        if (mutableData.getValue() != null) {
                            if (Integer.parseInt(room.playerOneClickAmount) > Integer.parseInt(room.playerTwoClickAmount)) {
                                int wins = Integer.parseInt(mutableData.child("gamesWon").getValue().toString());
                                mutableData.child("gamesWon").setValue("" + (wins + 1));
                            } else {
                                int losses = Integer.parseInt(mutableData.child("gamesLost").getValue().toString());
                                mutableData.child("gamesLost").setValue("" + (losses + 1));
                            }
                            return Transaction.success(mutableData);
                        }
                        return Transaction.success(mutableData);
                    }

                    @Override
                    public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {

                    }
                });
            } else {
                databaseReference.child("users").child(mAuth.getCurrentUser().getUid()).runTransaction(new Transaction.Handler() {
                    @Override
                    public Transaction.Result doTransaction(MutableData mutableData) {
                        if (mutableData.getValue() != null) {
                            if (Integer.parseInt(room.playerTwoClickAmount) > Integer.parseInt(room.playerOneClickAmount)) {
                                int wins = Integer.parseInt(mutableData.child("gamesWon").getValue().toString());
                                mutableData.child("gamesWon").setValue("" + (wins + 1));
                            } else {
                                int losses = Integer.parseInt(mutableData.child("gamesLost").getValue().toString());
                                mutableData.child("gamesLost").setValue("" + (losses + 1));
                            }
                            return Transaction.success(mutableData);
                        }
                        return Transaction.success(mutableData);
                    }

                    @Override
                    public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {

                    }
                });
            }
        }
        getWinnerInfo = true;
        displayData();
    }
}

package koiv.sven.clickwar;

import android.content.Context;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;

/**
 * Created by Sven Kõiv on 17.05.2017.
 */

class SoundHelper {

    private SoundPool mSoundPool;
    private int mClickSound;
    private Context mContext;

    SoundHelper(Context mContext) {
        this.mContext = mContext;
        createSoundPool();
        loadSound();
    }

    private void createSoundPool(){
        if (Build.VERSION.SDK_INT>= Build.VERSION_CODES.LOLLIPOP) {
            AudioAttributes.Builder attributeBuilder= new AudioAttributes.Builder();
            attributeBuilder.setUsage(AudioAttributes.USAGE_GAME);
            attributeBuilder.setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION);
            AudioAttributes attributes = attributeBuilder.build();

            SoundPool.Builder soundPoolBuilder = new SoundPool.Builder();
            soundPoolBuilder.setAudioAttributes(attributes);
            mSoundPool = soundPoolBuilder.build();
        } else {
            mSoundPool = new SoundPool(1, AudioManager.STREAM_MUSIC,0);
        }
    }

    private void loadSound() {
        mClickSound = mSoundPool.load(mContext, R.raw.click, 1);
    }

    void release() {
        mSoundPool.release();
    }

    void clickSound() {
        mSoundPool.play(mClickSound, 1, 1, 0, 0, 1);
    }
}

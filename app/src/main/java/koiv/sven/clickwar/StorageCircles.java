package koiv.sven.clickwar;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Sven Kõiv on 15.05.2017.
 */

class StorageCircles {

    private Circle mCircle;
    private Circle mOpponentCircle;
    private List<Circle> mMyCircleChildren = new ArrayList<>();
    private List<Circle> mOpponentCircleChildren = new ArrayList<>();
    private Random mRandom = new Random();
    private float mPixelDensity;
    private int mWidth;

    StorageCircles(Circle mCircle, Circle mOpponentCircle, float mPixelDensity, int mWidth) {
        this.mCircle = mCircle;
        this.mOpponentCircle = mOpponentCircle;
        this.mPixelDensity = mPixelDensity;
        this.mWidth = mWidth;
    }

    void giveBirthToMyChildCircle() {
        synchronized (mMyCircleChildren) {
            final int RADIUS = 5;
            int height = (int) mCircle.getLocationY() - mCircle.getCircleRadius() + mCircle.toDensity(RADIUS);
            int widthLeft = (int) mCircle.getLocationX() - (mCircle.toDensity(RADIUS) * 5);
            int widthRight = (int) mCircle.getLocationX() + (mCircle.toDensity(RADIUS) * 5);
            int randomWidth = mRandom.nextInt(widthRight - widthLeft) + widthLeft;
            Circle circle = new Circle(randomWidth, height, mPixelDensity, RADIUS);
            circle.setCircleMoveSpeed(mWidth);
            mMyCircleChildren.add(circle);
        }
    }

    void giveBirthToOpponentCircle() {
        synchronized (mOpponentCircleChildren) {
            final int RADIUS = 5;
            int height = (int) mOpponentCircle.getLocationY() + mCircle.getCircleRadius() - mCircle.toDensity(RADIUS);
            int widthLeft = (int) mOpponentCircle.getLocationX() - (mOpponentCircle.toDensity(RADIUS) * 5);
            int widthRight = (int) mOpponentCircle.getLocationX() + (mOpponentCircle.toDensity(RADIUS) * 5);
            int randomWidth = mRandom.nextInt(widthRight - widthLeft) + widthLeft;
            Circle circle = new Circle(randomWidth, height, mPixelDensity, RADIUS);
            circle.setCircleMoveSpeed(mWidth);
            mOpponentCircleChildren.add(circle);
        }
    }

    List<Circle> removeCirclesFromList(List<Circle> circlesToRemove, List<Circle> circlesList) {
        for (Circle circle : circlesToRemove) {
            circlesList.remove(circle);
        }
        return circlesList;
    }

    void setMyCircleChildren(List<Circle> myCircleChildren) {
        mMyCircleChildren = myCircleChildren;
    }

    void setOpponentCircleChildren(List<Circle> opponentCircleChildren) {
        mOpponentCircleChildren = opponentCircleChildren;
    }

    List<Circle> getMyCircleChildren() {
        return mMyCircleChildren;
    }

    List<Circle> getOpponentCircleChildren() {
        return mOpponentCircleChildren;
    }
}

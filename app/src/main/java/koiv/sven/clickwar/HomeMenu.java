package koiv.sven.clickwar;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

public class HomeMenu extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private static final String TAG = "HomeMenu";
    private static final String LOGGED_IN_FIRST_TIME = "LoggedInFirstTime";
    private ProgressBar mProgressLookingGame;
    private Button mPlayButton;
    private String mGameRoomID;
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    private DatabaseReference mJoinedGameRoomRef;
    private ValueEventListener mJoinedGameRoomListener;
    private boolean mLookingForGame;
    private User mUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_menu);
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        getUserInformation();
        firstTimeLoggedIn(mAuth.getCurrentUser()); // Create user in firebase if it's new

        mProgressLookingGame = (ProgressBar) findViewById(R.id.progressLookingGame);
        mPlayButton = (Button) findViewById(R.id.playButton);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_menu, menu);
        setNavigationDrawer();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.signOut) {
            signUserOut();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onPause() {
        mLookingForGame = false;
        if (mJoinedGameRoomListener != null) mJoinedGameRoomRef.removeEventListener(mJoinedGameRoomListener);
        if (mProgressLookingGame.getVisibility() == View.VISIBLE) {
            hideProgressLookingGame();
            removePlayerFromOpenRoom();
        }
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        removePlayerFromOpenRoom();
        super.onDestroy();
        Runtime.getRuntime().gc();
    }

    /**
     * Method firstTimeLoggedIn
     *      - Logic
     */
    private void firstTimeLoggedIn(FirebaseUser user) {
        if (isNewUser()) createNewUserInDatabase(user);
        else isUserInDatabase(user);
    }

    private void createNewUserInDatabase(FirebaseUser user) {
        User newUser = new User(user.getDisplayName(), user.getEmail(), "0", "0", "0", mAuth.getCurrentUser().getPhotoUrl().toString());
        mDatabase.child("users").child(user.getUid()).setValue(newUser);
    }

    private boolean isNewUser() {
        SharedPreferences pref = getSharedPreferences(LOGGED_IN_FIRST_TIME, Activity.MODE_PRIVATE);
        if (!pref.contains(LOGGED_IN_FIRST_TIME)) {
            pref.edit().putBoolean(LOGGED_IN_FIRST_TIME, true).apply();
            return true;
        }
        return false;
    }

    private void isUserInDatabase(final FirebaseUser user) {
        DatabaseReference userRef = mDatabase.child("users").child(user.getUid());
        userRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (!dataSnapshot.exists()) createNewUserInDatabase(user);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    private void setNavigationDrawer() {
        FirebaseUser user = mAuth.getCurrentUser();
        if (user != null) {
            setUserProfilePhoto(user);
            setUserProfileName(user);
        } else {
            Log.d(TAG, "userLoggedIn:false");
            signUserOut();
        }
    }

    private void setUserProfilePhoto(FirebaseUser user) {
        if (user.getPhotoUrl() != null) {
            Picasso.with(this)
                    .load(user.getPhotoUrl())
                    .transform(new CircleTransform()).into((ImageView) findViewById(R.id.profilePhoto));
        }
    }

    private void setUserProfileName(FirebaseUser user) {
        String profileName = user.getDisplayName();
        TextView profileNameHolder = (TextView) findViewById(R.id.profileName);
        profileNameHolder.setText(profileName);
    }

    private void signUserOut() {
        mAuth.signOut();
        finish();
        Intent intent = new Intent(this, LoginScreen.class);
        startActivity(intent);
    }

    private void showProgressLookingGame() {
        mProgressLookingGame.setVisibility(View.VISIBLE);
        mPlayButton.setText(R.string.btnStop);
    }

    private void hideProgressLookingGame() {
        mProgressLookingGame.setVisibility(View.INVISIBLE);
        mPlayButton.setText(R.string.btnPlay);
    }

    public void onPlayPressed(View v) {
        if (mProgressLookingGame.getVisibility() == View.INVISIBLE) {
            mLookingForGame = true;
            showProgressLookingGame();
            joinOpenRoom();
        } else {
            mLookingForGame = false;
            hideProgressLookingGame();
            removePlayerFromOpenRoom();
        }
    }

    private void removePlayerFromOpenRoom() {
        DatabaseReference roomRef = mDatabase.child("openRoom");
        roomRef.runTransaction(new Transaction.Handler() {

            @Override
            public Transaction.Result doTransaction(MutableData mutableData) {
                if (mutableData.getValue() != null) {
                    if (mutableData.child("playerTwo").getValue().equals(mAuth.getCurrentUser().getUid())) {
                        mutableData.child("playerTwo").setValue("");
                        mutableData.child("playerTwoStatus").setValue("");
                        return Transaction.success(mutableData);
                    } else if (mutableData.child("playerOne").getValue().equals(mAuth.getCurrentUser().getUid())) {
                        mutableData.child("playerOne").setValue("");
                        mutableData.child("playerOneStatus").setValue("");
                        return Transaction.success(mutableData);
                    }
                }
                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {
            }
        });
    }

    /**
     * Method joinOpenRoom
     *      - Creates room if doesn't exist
     *      - (Joins as player one) || (Joins as player two)
     *      - Calls getReadyForGame
     */
    private void joinOpenRoom() {
        DatabaseReference roomRef = mDatabase.child("openRoom");
        final DatabaseReference gamesRef = mDatabase.child("games");
        roomRef.runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData mutableData) {
                if (mutableData.getValue() != null) {
                    if (mutableData.child("playerOne").getValue().equals("")) {
                        mutableData.child("playerOne").setValue(mAuth.getCurrentUser().getUid());
                        mutableData.child("playerOneStatus").setValue(Room.WAITING);
                    } else if (mutableData.child("playerTwo").getValue().equals("")) {
                        mutableData.child("playerTwo").setValue(mAuth.getCurrentUser().getUid());
                        mutableData.child("playerTwoStatus").setValue(Room.WAITING);
                    } else if (mLookingForGame) {
                        joinOpenRoom();
                        Transaction.abort();
                    }
                    return Transaction.success(mutableData);
                }
                mutableData.setValue(new Room(mAuth.getCurrentUser().getUid(), "", Room.WAITING, "", "", gamesRef.push().getKey()));
                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {
                if (b) getReadyForGame(dataSnapshot);
            }
        });
    }

    /**
     * Method getReadyForGame
     *      - Creates 'games/{unique-room}'
     *      - Resets 'openRoom'
     *      - Sets players ready for game
     */
    private void getReadyForGame(DataSnapshot dataSnapshot) {
        Room room = dataSnapshot.getValue(Room.class);
        mGameRoomID = room.roomID;
        if (room.playerOneStatus.equals(Room.WAITING) && room.playerTwoStatus.equals(Room.WAITING)) {
            resetOpenRoom();
            createGameRoom(room);
        }
        setPlayersReady();
    }

    private void createGameRoom(Room room) {
        DatabaseReference gameRoomRef = mDatabase.child("games").child(mGameRoomID);
        gameRoomRef.setValue(room);
    }

    private void resetOpenRoom() {
        DatabaseReference roomRef = mDatabase.child("openRoom");
        roomRef.setValue(null);
    }

    private void setPlayersReady() {
        mJoinedGameRoomRef = mDatabase.child("games").child(mGameRoomID);
        mJoinedGameRoomListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() != null) {
                    Room room = dataSnapshot.getValue(Room.class);
                    if (room.playerOne.equals(mAuth.getCurrentUser().getUid())
                            && room.playerOneStatus.equals(Room.WAITING)) {
                        mJoinedGameRoomRef.child("playerOneStatus").setValue(Room.READY);
                    } else if (room.playerTwo.equals(mAuth.getCurrentUser().getUid())
                            && room.playerTwoStatus.equals(Room.WAITING)) {
                        mJoinedGameRoomRef.child("playerTwoStatus").setValue(Room.READY);
                    } else if (room.playerOneStatus.equals(Room.READY) &&
                            room.playerTwoStatus.equals(Room.READY)) {
                        startGame();
                    }
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        };
        mJoinedGameRoomRef.addValueEventListener(mJoinedGameRoomListener);
    }

    private void getUserInformation() {
        mDatabase.child("users").child(mAuth.getCurrentUser().getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot != null) {
                    mUser = dataSnapshot.getValue(User.class);
                    displayUserData();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @SuppressLint("SetTextI18n")
    private void displayUserData() {
        TextView gamesAmount = (TextView) findViewById(R.id.gamesAmount);
        TextView gamesWon = (TextView) findViewById(R.id.gamesWon);
        TextView gamesLost = (TextView) findViewById(R.id.gamesLost);

        gamesAmount.setText("GAMES " + (Integer.parseInt(mUser.gamesLost) + Integer.parseInt(mUser.gamesWon)));
        gamesWon.setText("WON " + mUser.gamesWon);
        gamesLost.setText("LOST " + mUser.gamesLost);
    }

    private void startGame() {
        hideProgressLookingGame();
        Intent intent = new Intent(this, Game.class);
        intent.putExtra("room", mGameRoomID);
        startActivity(intent);
    }
}

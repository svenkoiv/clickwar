package koiv.sven.clickwar;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by Sven Kõiv on 15.05.2017.
 */

@IgnoreExtraProperties
public class Effect {

    public String name;
    public String angle = "";
    public String playerClicked = "";

    public Effect() {};

    public Effect(String name, String angle) {
        this.name = name;
        this.angle = angle;
    }
}

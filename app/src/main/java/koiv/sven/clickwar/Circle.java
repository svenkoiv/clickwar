package koiv.sven.clickwar;

import android.util.Log;

/**
 * Created by Sven Kõiv on 5.05.2017.
 */

class Circle {

    private int radius;
    private double speed;
    private int angle = 35;
    private int circleX;
    private int circleY;
    private double density;

    Circle(int circleX, int circleY, double density, int radius) {
        this.density = density;
        this.circleX = circleX;
        this.circleY = circleY;
        this.radius = radius;
    }

    void setCircleMoveSpeed(int screenWidth) {
        speed = screenWidth / 200;
    }

    void moveUp() {
        circleY += -1 * Math.abs(speed);
    }

    void moveDown() {
        circleY += speed;
    }

    boolean isCircleTouched(int clickX, int clickY) {
        return (clickX >= circleX - getCircleRadius() && clickX <= circleX + getCircleRadius() &&
                clickY >= circleY - getCircleRadius() && clickY <= circleY + getCircleRadius());
    }

    int getCircleRadius() {
        return toDensity(radius);
    }

    void setLocationY(int h, boolean isOpponent) {
        if (isOpponent) circleY = h + getCircleRadius() + toDensity(20);
        else circleY = h - getCircleRadius() - toDensity(20);
    }

    float getLocationY() {
        return (float)circleY;
    }

    void setLocationX(int w) {
        circleX = w;
    }

    float getLocationX() {
        return (float)circleX;
    }

    int toDensity(int pixels) {
        return (int)((pixels * density));
    }
}

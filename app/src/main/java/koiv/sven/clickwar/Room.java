package koiv.sven.clickwar;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Sven Kõiv on 28.04.2017.
 */

@IgnoreExtraProperties
public class Room {

    final static String READY = "ready";
    final static String GAME_OVER = "game_over";
    final static String WAITING = "waiting";
    final static String EMPTY = "empty";
    final static String PLAY = "play";
    public String playerOne;
    public String playerLeft = "";
    public String playerTwo;
    public String playerOneStatus;
    public String playerTwoStatus;
    public String roomID;
    public String roomStatus;
    public String playerOneClickAmount = "0";
    public String playerTwoClickAmount = "0";


    public Room() {}

    public Room(String playerOne, String playerTwo, String playerOneStatus, String playerTwoStatus, String roomStatus, String roomID) {
        this.playerOne = playerOne;
        this.playerTwo = playerTwo;
        this.playerOneStatus = playerOneStatus;
        this.playerTwoStatus = playerTwoStatus;
        this.roomStatus = roomStatus;
        this.roomID = roomID;
    }
}

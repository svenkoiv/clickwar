package koiv.sven.clickwar;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;

/**
 * Created by Sven Kõiv on 14.05.2017.
 */

class FirebaseTools {

    Transaction.Handler getSendClickTransactionPlayerOne() {
        return new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData mutableData) {
                if (mutableData.getValue() != null) {
                    int playerOneClickAmount = Integer.parseInt("" + mutableData.getValue());
                    mutableData.setValue("" + (playerOneClickAmount + 1));
                    return Transaction.success(mutableData);
                }
                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {}
        };
    }

    Transaction.Handler getSendClickTransactionPlayerTwo() {
        return new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData mutableData) {
                if (mutableData.getValue() != null) {
                    int playerTwoClickAmount = Integer.parseInt("" + mutableData.getValue());
                    mutableData.setValue("" + (playerTwoClickAmount + 1));
                    return Transaction.success(mutableData);
                }
                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {}
        };
    }
}

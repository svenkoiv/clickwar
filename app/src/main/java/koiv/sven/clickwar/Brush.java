package koiv.sven.clickwar;

import android.graphics.Color;
import android.graphics.Paint;

/**
 * Created by Sven Kõiv on 14.05.2017.
 */

class Brush {

    private float density;

    Brush(float density) {
        this.density = density;
    }

    Paint getMyCirclePaint() {
        Paint paint = new Paint();
        paint.setColor(Color.parseColor("#7b7bff"));
        paint.setAntiAlias(true);
        return paint;
    }

    Paint getOpponentPaint() {
        Paint color = new Paint();
        color.setColor(Color.parseColor("#03b05d"));
        color.setAntiAlias(true);
        return color;
    }

    Paint getStartTextPaint(int size) {
        Paint textPaint = new Paint();
        textPaint.setColor(Color.parseColor("#fafafa"));
        textPaint.setTextAlign(Paint.Align.CENTER);
        textPaint.setAntiAlias(true);
        textPaint.setTextSize(toDensity(size));
        return textPaint;
    }

    Paint getTextPaint(int size) {
        Paint textPaint = new Paint();
        textPaint.setColor(Color.parseColor("#acacac"));
        textPaint.setTextAlign(Paint.Align.CENTER);
        textPaint.setAntiAlias(true);
        textPaint.setTextSize(toDensity(size));
        return textPaint;
    }

    Paint getBackgroundColorInt() {
        Paint black = new Paint();
        black.setColor(Color.parseColor("#464646"));
        return black;
    }

    private int toDensity(int pixels) {
        return (int) (pixels * density);
    }

}

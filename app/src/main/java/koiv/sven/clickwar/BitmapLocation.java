package koiv.sven.clickwar;

import android.graphics.Bitmap;

/**
 * Created by Sven Kõiv on 15.05.2017.
 */

class BitmapLocation {

    private Integer locationX;
    private Integer locationY;
    private Bitmap bitmap;
    private int angle = 35;
    private int speed;

    BitmapLocation(Bitmap bitmap, int angle) {
        this.bitmap = bitmap;
        this.angle = angle;
    }

    Integer getLocationX() {
        return locationX;
    }

    Integer getLocationY() {
        return locationY;
    }

    void setLocationX(int locationX) {
        this.locationX = locationX;
    }

    void setLocationY(int locationY) {
        this.locationY = locationY;
    }

    int getWidth() {
        return bitmap.getWidth();
    }

    int getHeight() {
        return bitmap.getHeight();
    }

    void calculateNewPosition() {
        double radians = angle * Math.PI / 180;
        locationX += (int) (Math.cos(radians) * speed);
        locationY += (int) (Math.sin(radians) * speed);
    }

    void setCircleMoveSpeed(int screenWidth) {
        speed = screenWidth / 100;
    }

    void moveBitmap(int canvasWidth, int canvasHeight) {
        setCircleMoveSpeed(canvasWidth);
        calculateNewPosition();
        if (locationX + getWidth() > canvasWidth || locationX < 0) {
            angle = 180 - angle;
        }
        if (locationY + getHeight() > canvasHeight || locationY < 0) {
            angle = 360 - angle;
        }
    }

    Bitmap getBitmap() {
        return bitmap;
    }
}

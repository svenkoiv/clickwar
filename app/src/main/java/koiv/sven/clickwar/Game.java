package koiv.sven.clickwar;

import android.content.Intent;
import android.media.AudioManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;

public class Game extends AppCompatActivity {

    public static final String TAG = "Game";
    private DatabaseReference mRoomReference;
    private FirebaseAuth mAuth;
    private GameLayoutView gameLayoutView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getRoomRefFromIntent();
        mAuth = FirebaseAuth.getInstance();
        gameLayoutView = new GameLayoutView(this, mRoomReference);
        setVolumeControlStream(AudioManager.STREAM_MUSIC);
        setContentView(gameLayoutView);
    }

    @Override
    protected void onResume() {
        super.onResume();
        gameLayoutView.resume();
    }

    @Override
    protected void onPause() {
        gameLayoutView.pause();
        super.onPause();
    }

    private void getRoomRefFromIntent() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            String room = bundle .getString("room");
            Log.d(TAG, room);
            if (room == null) Log.d(TAG, "getRoomRefFromIntent:NoRoom");
            else mRoomReference = FirebaseDatabase.getInstance().getReference().child("games").child(room);
        }
    }
}

package koiv.sven.clickwar;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {

    private Circle circle;

    @Before
    public void setUp() throws Exception {
        circle = new Circle(50, 50, 3);
        circle.setCircleMoveSpeed(1080);
    }

    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }
}